# LBC - Test

# Features

- List of all albums

# Architecture

The project has 3 main packages :

  - App : Hosting presentation and UI and view state logic.
  - Domain : Hosting domain models and business rules transformation.
  - Data : Hosting data layer models and transformation to domain models, 
  as well as data querying and network related logic.

# Dependencies

  - RxJava2 : For reactive streaming and asynchronous operations (I've been using Rx for years now, I feel more comfortable with Rx than I am with Coroutines)
  - Retrofit : As Http Client (Used almost everywhere, easy to use, work fine with Rx)
  - Room : As persistence library
  - Koin : for dependency injection. (Simple and quick to setup)
  - Picasso : For Image loading
  
# Testing

  - JUnit 4 : Testing framework.
  - Mokito : Mocking framework.
  - Truth : Test assertions.
  - MockWebserver : A webserver for testing api layer.
