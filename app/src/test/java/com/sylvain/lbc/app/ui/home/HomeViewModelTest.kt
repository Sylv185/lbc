package com.sylvain.lbc.app.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.sylvain.lbc.app.rx.AppSchedulers
import com.sylvain.lbc.app.rx.initForTests
import com.sylvain.lbc.domain.album.Album
import com.sylvain.lbc.domain.album.GetAlbumListUseCase
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Mock
    lateinit var getAlbumUseCase: GetAlbumListUseCase

    @Mock
    lateinit var mockObserver: Observer<ViewState>

    private lateinit var viewModel: HomeViewModel

    @Before
    fun before() {
        AppSchedulers.initForTests()
    }

    @Test
    fun `should load albums`() {
        // Given
        given(getAlbumUseCase.execute(any())).willReturn(
            Single.just(Result.success(listOf(Album(id = 1, itemList = listOf()))))
        )
        viewModel = HomeViewModel(getAlbumUseCase)

        // When
        viewModel.distinctViewState.observeForever(mockObserver)
        viewModel.action(LoadAlbumList())

        // Then
        Truth.assertThat(viewModel.distinctViewState.value?.albumList).isEqualTo(listOf(AlbumUi(albumId = 1)))
    }
}
