package com.sylvain.lbc.domain.album

import com.nhaarman.mockitokotlin2.any
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetAlbumListUseCaseTest {

    private lateinit var useCase: GetAlbumListUseCase

    @Mock
    private lateinit var repository: AlbumRepository

    @Before
    fun before() {
        useCase = GetAlbumListUseCase(repository)
    }

    @Test
    fun `should get albums from repo`() {
        // Given
        given(repository.getAlbumList()).willReturn(Single.just(albumList))

        // When
        val testObserver = useCase.execute(param).test()

        // Then
        testObserver.assertComplete()
            .assertValueAt(0) { it.isSuccess }
            .assertResult(successResult)
    }

    @Test
    fun `should fail getting albums from repo`() {
        // Given
        given(repository.getAlbumList()).willReturn(Single.error(throwable))

        // When
        val testObserver = useCase.execute(param).test()

        // Then
        testObserver.assertComplete()
            .assertNoErrors()
            .assertResult(failureResult)
    }

    // ================================================
    // Data
    // ================================================

    private val albumList = listOf(Album(id = 1, itemList = listOf()))
    private val successResult = Result.success(albumList)
    private val throwable = Throwable("error")
    private val failureResult = Result.failure<List<Album>>(throwable)
    private val param = GetAlbumListUseCaseParam(forceRefresh = false)

}
