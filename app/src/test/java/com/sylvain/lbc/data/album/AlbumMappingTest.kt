package com.sylvain.lbc.data.album

import com.google.common.truth.Truth
import com.sylvain.lbc.data.album.dao.AlbumItemEntity
import org.junit.Test

class AlbumMappingTest {

    @Test
    fun `should map entity list to 3 albums`() {
        // When
        val albumList = entityList.toAlbumList()

        //Then
        Truth.assertThat(albumList.size).isEqualTo(3)
        Truth.assertThat(albumList[0].itemList.size).isEqualTo(2)
        Truth.assertThat(albumList[1].itemList.size).isEqualTo(1)
        Truth.assertThat(albumList[2].itemList.size).isEqualTo(2)
    }

    private val albumItemEntity = AlbumItemEntity(
        albumId = 1,
        id = 1,
        title = "title1",
        url = "url",
        thumbnailUrl = "thurl"
    )

    private val entityList = listOf(
        albumItemEntity.copy(albumId = 1, id = 2),
        albumItemEntity.copy(albumId = 1, id = 3),
        albumItemEntity.copy(albumId = 2, id = 4),
        albumItemEntity.copy(albumId = 3, id = 5),
        albumItemEntity.copy(albumId = 3, id = 6)
    )
}
