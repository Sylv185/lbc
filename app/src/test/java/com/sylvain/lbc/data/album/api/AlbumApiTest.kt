package com.sylvain.lbc.data.album.api

import com.sylvain.lbc.data.testNetworkModule
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class AlbumApiTest : KoinTest {

    private val mockServer = MockWebServer()
    private val okHttpClient: OkHttpClient by inject()
    private val gsonConverterFactory: GsonConverterFactory by inject()

    private lateinit var albumApi: AlbumApi

    @Before
    fun before() {
        val response = MockResponse()
            .setBody(
                AlbumApiTest::class.java.getResource("/album_api_response.json")!!.readText()
            )
        mockServer.enqueue(response)
    }

    @After
    fun after() {
        mockServer.shutdown()
    }

    @Test
    fun testApiCall() {
        // Given
        startKoin { modules(testNetworkModule) }
        albumApi = Retrofit.Builder()
            .baseUrl(mockServer.url("").toString())
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(AlbumApi::class.java)
        // When
        val albumListTestObserver = albumApi.getAlbumList().test()

        // Then
        albumListTestObserver.assertComplete()
            .assertValueCount(1)
            .assertValue { result -> result.size == 2 }

    }

}