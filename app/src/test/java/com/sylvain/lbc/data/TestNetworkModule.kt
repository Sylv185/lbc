package com.sylvain.lbc.data

import com.google.gson.GsonBuilder
import com.sylvain.lbc.data.album.api.AlbumApi
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val testNetworkModule = module {

    single<GsonConverterFactory> { GsonConverterFactory.create() }
    single { GsonBuilder().create() }
    single<OkHttpClient> { buildOkHttpClient() }

    single {
        buildRetrofit(
            baseUrl = get<MockWebServer>().url("").toString(),
            okHttpClient = get(),
            gsonConverterFactory = get()
        ).create(AlbumApi::class.java)
    }
}

private fun buildRetrofit(
    baseUrl: String,
    okHttpClient: OkHttpClient,
    gsonConverterFactory: GsonConverterFactory
) =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(gsonConverterFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

private fun buildOkHttpClient() = OkHttpClient()
    .newBuilder()
    .build()
