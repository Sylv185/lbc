package com.sylvain.lbc.data.album

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.sylvain.lbc.data.album.api.AlbumApi
import com.sylvain.lbc.data.album.api.AlbumItemJson
import com.sylvain.lbc.data.album.dao.AlbumDao
import com.sylvain.lbc.domain.album.Album
import com.sylvain.lbc.domain.album.AlbumItem
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NetworkAlbumRepositoryTest {

    @Mock
    private lateinit var albumApi: AlbumApi

    @Mock
    private lateinit var albumDao: AlbumDao

    private lateinit var repository: NetworkAlbumRepository

    @Before
    fun before() {
        repository = NetworkAlbumRepository(albumApi, albumDao)
    }

    @Test
    fun `should load, store and get from DB`() {
        // Given
        given(albumApi.getAlbumList()).willReturn(Single.just(albumJsonList))
        given(albumDao.deleteAlbumItems()).willReturn(Single.just(0))
        given(albumDao.insertAll(albumJsonList.toAlbumItemEntityList())).willReturn(Single.just(listOf(1L)))
        given(albumDao.getAllItems()).willReturn(Single.just(albumJsonList.toAlbumItemEntityList()))

        // When
        val testObserver = repository.getAlbumList(forceRefresh = true).test()

        // Then
        testObserver.assertComplete()
            .assertResult(albumList)

        verify(albumApi).getAlbumList()
        verify(albumDao).deleteAlbumItems()
        verify(albumDao).insertAll(any())
        verify(albumDao).getAllItems()
    }

    @Test
    fun `should load album from DB`() {
        // Given
        given(albumDao.count()).willReturn(Single.just(1))
        given(albumDao.getAllItems()).willReturn(Single.just(albumJsonList.toAlbumItemEntityList()))

        // When
        val testObserver = repository.getAlbumList(forceRefresh = false).test()

        // Then
        testObserver.assertComplete()
            .assertResult(albumList)

        verify(albumApi, never()).getAlbumList()
        verify(albumDao).getAllItems()
    }

    @Test
    fun `should load album from DB because of api error`() {
        // Given
        given(albumApi.getAlbumList()).willReturn(Single.error(Throwable("error")))
        given(albumDao.getAllItems()).willReturn(Single.just(albumJsonList.toAlbumItemEntityList()))

        // When
        val testObserver = repository.getAlbumList(forceRefresh = true).test()

        // Then
        testObserver.assertComplete()
            .assertResult(albumList)

        verify(albumApi).getAlbumList()
        verify(albumDao).getAllItems()
    }

    @Test
    fun `should load album items from network because db is empty`() {
        // Given
        given(albumDao.count()).willReturn(Single.just(0))
        given(albumApi.getAlbumList()).willReturn(Single.just(albumJsonList))
        given(albumDao.deleteAlbumItems()).willReturn(Single.just(0))
        given(albumDao.insertAll(albumJsonList.toAlbumItemEntityList())).willReturn(Single.just(listOf(1L)))
        given(albumDao.getAllItems()).willReturn(Single.just(albumJsonList.toAlbumItemEntityList()))

        // When
        val testObserver = repository.getAlbumList(forceRefresh = false).test()

        // Then
        testObserver.assertComplete()
            .assertResult(albumList)

        verify(albumApi).getAlbumList()
        verify(albumDao).deleteAlbumItems()
        verify(albumDao).insertAll(any())
        verify(albumDao).getAllItems()
        verify(albumDao).count()
    }

    private val albumJsonList =
        listOf(AlbumItemJson(albumId = 1, id = 1, url = "url1", title = "title1", thumbnailUrl = "url1"))
    private val albumList = listOf(
        Album(
            id = 1,
            itemList = listOf(AlbumItem(albumId = 1, id = 1, url = "url1", title = "title1", thumbnailUrl = "url1"))
        )
    )
}
