package com.sylvain.lbc.app.ui.base

abstract class BaseViewModelWithViewEffect<ViewStateT : Any, ActionT : Any, ViewEffectT : Any> :
    BaseViewModel<ViewStateT, ActionT>() {

    private val viewEffectLiveData = SingleLiveEvent<ViewEffectT>()

    fun getViewEffect(): SingleLiveEvent<ViewEffectT> = viewEffectLiveData

    protected fun postViewEffect(viewEffect: ViewEffectT) {
        viewEffectLiveData.value = viewEffect
    }
}
