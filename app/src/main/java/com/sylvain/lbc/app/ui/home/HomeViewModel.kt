package com.sylvain.lbc.app.ui.home

import com.sylvain.lbc.app.rx.AppSchedulers
import com.sylvain.lbc.app.ui.base.BaseViewModelWithViewEffect
import com.sylvain.lbc.domain.album.GetAlbumListUseCase
import com.sylvain.lbc.domain.album.GetAlbumListUseCaseParam
import io.reactivex.rxkotlin.plusAssign

class HomeViewModel(private val getAlbumListUseCase: GetAlbumListUseCase) :
    BaseViewModelWithViewEffect<ViewState, Action, ViewEffect>() {

    init {
        action(LoadAlbumList())
    }

    override fun makeDefaultViewState() = ViewState()

    override fun action(action: Action) {
        super.action(action)
        when (action) {
            is LoadAlbumList -> onLoadAlbum(action.forceRefresh)
        }
    }

    private fun onLoadAlbum(forceRefresh: Boolean) {
        postViewState(viewState.copy(isLoading = true))
        disposable += getAlbumListUseCase.execute(GetAlbumListUseCaseParam(forceRefresh = forceRefresh))
            .subscribeOn(AppSchedulers.io())
            .observeOn(AppSchedulers.mainThread())
            .subscribe { result ->
                if (result.isFailure) {
                    postViewEffect(ShowError("Une erreur est survenue"))
                } else {
                    postViewState(viewState.copy(isLoading = false, albumList = result.getOrNull()?.toAlbumListItemUi() ?: emptyList()))
                }
            }
    }
}
