package com.sylvain.lbc.app

import android.app.Application
import com.sylvain.lbc.BuildConfig
import com.sylvain.lbc.app.di.appModule
import com.sylvain.lbc.app.di.dataModule
import com.sylvain.lbc.app.di.domainModule
import com.sylvain.lbc.app.di.networkModule
import com.sylvain.lbc.app.rx.AppSchedulers
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class LbcApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
        initKoin()
        AppSchedulers.init()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@LbcApp)
            modules(appModule, domainModule, dataModule, networkModule)
        }
    }
}
