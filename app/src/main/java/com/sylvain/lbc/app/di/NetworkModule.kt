package com.sylvain.lbc.app.di

import com.google.gson.GsonBuilder
import com.sylvain.lbc.R
import com.sylvain.lbc.data.album.api.AlbumApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single<GsonConverterFactory> { GsonConverterFactory.create() }
    single { GsonBuilder().create() }
    single<OkHttpClient> { buildOkHttpClient() }

    single {
        buildRetrofit(
            baseUrl = androidContext().getString(R.string.lbc_base_url),
            okHttpClient = get(),
            gsonConverterFactory = get()
        ).create(AlbumApi::class.java)
    }
}

private fun buildRetrofit(
    baseUrl: String,
    okHttpClient: OkHttpClient,
    gsonConverterFactory: GsonConverterFactory
) =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(gsonConverterFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

private fun buildOkHttpClient() = OkHttpClient()
    .newBuilder()
//    .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
    .build()
