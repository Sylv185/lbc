package com.sylvain.lbc.app.di

import com.sylvain.lbc.domain.album.GetAlbumListUseCase
import org.koin.dsl.module

val domainModule = module {
    single { GetAlbumListUseCase(get()) }
}