package com.sylvain.lbc.app.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

abstract class BaseViewModel<ViewStateT : Any, ActionT : Any> : ViewModel() {

    protected val disposable = CompositeDisposable()

    private val viewStateLiveData = MutableLiveData<ViewStateT>()
    protected val viewState: ViewStateT
        get() = viewStateLiveData.value ?: makeDefaultViewState()

    protected abstract fun makeDefaultViewState(): ViewStateT

    val distinctViewState: LiveData<ViewStateT> =
        Transformations.distinctUntilChanged(viewStateLiveData)

    protected fun postViewState(viewState: ViewStateT) {
        viewStateLiveData.value = viewState
    }

    open fun action(action: ActionT) {
        Timber.i("action: $action")
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}