package com.sylvain.lbc.app.ui.home

import com.sylvain.lbc.domain.album.Album
import com.sylvain.lbc.domain.album.AlbumItem

data class ViewState(
    val isLoading: Boolean = false,
    val albumList: List<AlbumListItemUi> = emptyList()
)

open class ViewEffect
data class ShowError(val message: String) : ViewEffect()

open class Action
data class LoadAlbumList(val forceRefresh: Boolean = false) : Action()

open class AlbumListItemUi
data class AlbumUi(val albumId: Int) : AlbumListItemUi()
data class AlbumItemUi(val item: AlbumItem) : AlbumListItemUi()

fun List<Album>.toAlbumListItemUi(): List<AlbumListItemUi> {
    val uiItems = mutableListOf<AlbumListItemUi>()
    this.forEach {
        uiItems.add(AlbumUi(it.id))
        uiItems.addAll(it.itemList.map { item -> AlbumItemUi(item = item) })
    }
    return uiItems
}