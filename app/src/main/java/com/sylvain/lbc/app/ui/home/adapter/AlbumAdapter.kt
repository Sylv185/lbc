package com.sylvain.lbc.app.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.sylvain.lbc.R
import com.sylvain.lbc.app.ui.home.AlbumItemUi
import com.sylvain.lbc.app.ui.home.AlbumListItemUi
import com.sylvain.lbc.app.ui.home.AlbumUi
import com.sylvain.lbc.databinding.ItemAlbumBinding
import com.sylvain.lbc.databinding.ItemAlbumItemBinding

class AlbumAdapter : ListAdapter<AlbumListItemUi, RecyclerView.ViewHolder>(AlbumDiffUtilCallback()) {

    companion object {
        const val ALBUM_TYPE = 1
        const val ALBUM_ITEM_TYPE = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ALBUM_TYPE -> {
                val binding = ItemAlbumBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                AlbumViewHolder(binding)
            }
            else -> {
                val binding = ItemAlbumItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                AlbumItemViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = getItem(position)) {
            is AlbumUi -> (holder as? AlbumViewHolder)?.bind(item)
            is AlbumItemUi -> (holder as? AlbumItemViewHolder)?.bind(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is AlbumUi -> ALBUM_TYPE
            else -> ALBUM_ITEM_TYPE
        }
    }
}

class AlbumDiffUtilCallback : DiffUtil.ItemCallback<AlbumListItemUi>() {

    override fun areItemsTheSame(oldItem: AlbumListItemUi, newItem: AlbumListItemUi): Boolean {
        if (oldItem is AlbumUi && newItem is AlbumUi) {
            return oldItem.albumId == newItem.albumId
        }

        if (oldItem is AlbumItemUi && newItem is AlbumItemUi) {
            return oldItem.item.id == newItem.item.id
        }

        return false
    }

    override fun areContentsTheSame(oldItem: AlbumListItemUi, newItem: AlbumListItemUi): Boolean {
        if (oldItem is AlbumUi && newItem is AlbumUi) {
            return oldItem == newItem
        }

        if (oldItem is AlbumItemUi && newItem is AlbumItemUi) {
            return oldItem == newItem
        }
        return false
    }
}

class AlbumViewHolder(
    private val binding: ItemAlbumBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(album: AlbumUi) {
        binding.albumTitle.text = binding.root.resources.getString(R.string.album_name, album.albumId.toString())
    }
}

class AlbumItemViewHolder(
    private val binding: ItemAlbumItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(album: AlbumItemUi) {
        Picasso.get()
            .load(album.item.thumbnailUrl)
            .placeholder(R.color.colorPrimaryEighty)
            .into(binding.image)
    }
}
