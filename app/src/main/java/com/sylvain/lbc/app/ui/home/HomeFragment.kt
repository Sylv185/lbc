package com.sylvain.lbc.app.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.sylvain.lbc.app.ui.home.adapter.AlbumAdapter
import com.sylvain.lbc.databinding.FragmentHomeBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
        private const val COLUMN_DISPLAY = 3
    }

    // ================================================
    // Fields
    // ================================================

    private val viewModel: HomeViewModel by viewModel()

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val adapter = AlbumAdapter()

    // ================================================
    // Lifecycle
    // ================================================

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAlbumList()
        initView()
        viewModel.distinctViewState.observe(viewLifecycleOwner, Observer { updateView(it) })
        viewModel.getViewEffect().observe(viewLifecycleOwner, Observer { handleEffect(it) })
    }

    // ================================================
    // Private
    // ================================================

    private fun initView() {
        binding.swipeRefreshLayout.setOnRefreshListener { viewModel.action(LoadAlbumList(forceRefresh = true)) }
    }

    private fun setUpAlbumList() {
        binding.albumList.adapter = adapter
        binding.albumList.layoutManager = GridLayoutManager(context, COLUMN_DISPLAY).apply {
            spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (adapter.getItemViewType(position)) {
                        AlbumAdapter.ALBUM_TYPE -> COLUMN_DISPLAY
                        else -> 1
                    }
                }
            }
        }
    }

    private fun updateView(viewState: ViewState) {
        adapter.submitList(viewState.albumList)
        binding.swipeRefreshLayout.isRefreshing = viewState.isLoading
    }

    private fun handleEffect(viewEffect: ViewEffect) {
        when (viewEffect) {
            is ShowError -> Toast.makeText(context, viewEffect.message, Toast.LENGTH_LONG).show()
        }
    }
}