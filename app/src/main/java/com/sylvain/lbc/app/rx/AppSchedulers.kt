package com.sylvain.lbc.app.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object AppSchedulers {

    private lateinit var trampoline: Scheduler
    private lateinit var io: Scheduler
    private lateinit var computation: Scheduler
    private lateinit var mainThread: Scheduler

    fun init(
        io: Scheduler = Schedulers.io(),
        computation: Scheduler = Schedulers.computation(),
        trampoline: Scheduler = Schedulers.trampoline(),
        mainThread: Scheduler = AndroidSchedulers.mainThread()
    ) {
        AppSchedulers.io = io
        AppSchedulers.computation = computation
        AppSchedulers.trampoline = trampoline
        AppSchedulers.mainThread = mainThread
    }

    fun io() = io
    fun computation() = io
    fun mainThread() = mainThread
    fun trampoline() = trampoline
}

fun AppSchedulers.initForTests() {
    init(
        io = Schedulers.trampoline(),
        computation = Schedulers.trampoline(),
        trampoline = Schedulers.trampoline(),
        mainThread = Schedulers.trampoline()
    )
}