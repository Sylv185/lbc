package com.sylvain.lbc.app.di

import androidx.room.Room
import com.sylvain.lbc.data.album.NetworkAlbumRepository
import com.sylvain.lbc.data.db.LbcDatabase
import com.sylvain.lbc.domain.album.AlbumRepository
import org.koin.dsl.module

val dataModule = module {
    single<AlbumRepository> { NetworkAlbumRepository(albumApi = get(), albumDao = get()) }

    single { Room.databaseBuilder(get(), LbcDatabase::class.java, "lbc-db").build() }
    single { get<LbcDatabase>().albumDao() }
}
