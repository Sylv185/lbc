package com.sylvain.lbc.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sylvain.lbc.data.album.dao.AlbumDao
import com.sylvain.lbc.data.album.dao.AlbumItemEntity

@Database(entities = [AlbumItemEntity::class], version = 1)
abstract class LbcDatabase : RoomDatabase() {
    abstract fun albumDao(): AlbumDao
}
