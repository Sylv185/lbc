package com.sylvain.lbc.data.album.api

import io.reactivex.Single
import retrofit2.http.GET

interface AlbumApi {

    @GET("/img/shared/technical-test.json")
    fun getAlbumList(): Single<List<AlbumItemJson>>
}
