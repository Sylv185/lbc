package com.sylvain.lbc.data.album.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface AlbumDao {

    @Query("SELECT COUNT(*) FROM album_item")
    fun count(): Single<Int>

    @Query("SELECT * FROM album_item")
    fun getAllItems(): Single<List<AlbumItemEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(itemList: List<AlbumItemEntity>): Single<List<Long>>

    @Query("DELETE FROM album_item")
    fun deleteAlbumItems(): Single<Int>
}
