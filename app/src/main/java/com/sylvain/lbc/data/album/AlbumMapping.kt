package com.sylvain.lbc.data.album

import com.sylvain.lbc.data.album.api.AlbumItemJson
import com.sylvain.lbc.data.album.dao.AlbumItemEntity
import com.sylvain.lbc.domain.album.Album
import com.sylvain.lbc.domain.album.AlbumItem

fun List<AlbumItemJson>.toAlbumItemEntityList() = map { it.toAlbumItemEntity() }

fun AlbumItemJson.toAlbumItemEntity() = AlbumItemEntity(
    albumId = albumId,
    id = id,
    thumbnailUrl = thumbnailUrl,
    title = title,
    url = url
)

fun List<AlbumItemEntity>.toAlbumList() =
    groupBy { it.albumId }
        .entries.map {
            Album(
                id = it.key,
                itemList = it.value.map { json -> json.toTrack() }
            )
        }

fun AlbumItemEntity.toTrack() = AlbumItem(
    albumId = albumId,
    id = id,
    thumbnailUrl = thumbnailUrl,
    title = title,
    url = url
)
