package com.sylvain.lbc.data.album

import com.sylvain.lbc.data.album.api.AlbumApi
import com.sylvain.lbc.data.album.dao.AlbumDao
import com.sylvain.lbc.data.album.dao.AlbumItemEntity
import com.sylvain.lbc.domain.album.Album
import com.sylvain.lbc.domain.album.AlbumRepository
import io.reactivex.Single

class NetworkAlbumRepository(
    private val albumApi: AlbumApi,
    private val albumDao: AlbumDao
) : AlbumRepository {

    override fun getAlbumList(forceRefresh: Boolean): Single<List<Album>> {

        if (!forceRefresh) {
            return albumDao.count().flatMap { count ->
                if (count == 0) getAlbumItemsFromNetwork()
                else getAlbumItemsFromDB().map { it.toAlbumList() }
            }
        }
        return getAlbumItemsFromNetwork()
    }

    private fun getAlbumItemsFromNetwork(): Single<List<Album>> {
        return albumApi.getAlbumList()
            .flatMap { saveAlbumItemsInDB(it.toAlbumItemEntityList()) }
            .flatMap { getAlbumItemsFromDB() }
            .onErrorResumeNext { getAlbumItemsFromDB() }
            .map { it.toAlbumList() }
    }

    private fun saveAlbumItemsInDB(albumItems: List<AlbumItemEntity>): Single<List<Long>> =
        albumDao.deleteAlbumItems()
            .flatMap { albumDao.insertAll(albumItems) }

    private fun getAlbumItemsFromDB(): Single<List<AlbumItemEntity>> {
        return albumDao.getAllItems()
    }
}
