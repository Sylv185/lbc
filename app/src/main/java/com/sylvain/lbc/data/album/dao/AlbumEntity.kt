package com.sylvain.lbc.data.album.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "album_item")
data class AlbumItemEntity(
    @PrimaryKey
    val id: Int,
    val albumId: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)
