package com.sylvain.lbc.domain.album

import com.sylvain.lbc.domain.BaseUseCase
import io.reactivex.Single

class GetAlbumListUseCase(private val repository: AlbumRepository) :
    BaseUseCase<GetAlbumListUseCaseParam, List<Album>>() {

    override fun doJob(param: GetAlbumListUseCaseParam): Single<List<Album>> {
        return repository.getAlbumList(forceRefresh = param.forceRefresh)
    }
}

data class GetAlbumListUseCaseParam(val forceRefresh : Boolean)