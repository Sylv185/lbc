package com.sylvain.lbc.domain

import io.reactivex.Single
import timber.log.Timber

abstract class BaseUseCase<ParamT, ResultT> {

    fun execute(param: ParamT): Single<Result<ResultT>> {
        return Single.create<Result<ResultT>> { emitter ->
            doJob(param).subscribe(
                {
                    emitter.onSuccess(Result.success(it))
                },
                {
                    Timber.e(it)
                    emitter.onSuccess(Result.failure(it))
                })
        }
    }

    protected abstract fun doJob(param: ParamT): Single<ResultT>
}
