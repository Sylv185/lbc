package com.sylvain.lbc.domain.album

import io.reactivex.Single

interface AlbumRepository {
    fun getAlbumList(forceRefresh: Boolean = false): Single<List<Album>>
}
