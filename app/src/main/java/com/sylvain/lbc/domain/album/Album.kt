package com.sylvain.lbc.domain.album

data class Album(
    val id: Int,
    val itemList: List<AlbumItem>
)

data class AlbumItem(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)