package com.sylvain.lbc.data.album.dao

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sylvain.lbc.data.db.LbcDatabase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AlbumDaoTest {

    private lateinit var db: LbcDatabase
    private lateinit var albumDao: AlbumDao


    @Before
    fun setUp() {
        db = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            LbcDatabase::class.java
        )
            .build()
        albumDao = db.albumDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun should_insert_and_retrieve_items() {
        // Given
        val albumItemList: List<AlbumItemEntity> = listOf(
            item.copy(id = 1, albumId = 1),
            item.copy(id = 2, albumId = 1),
            item.copy(id = 3, albumId = 2),
            item.copy(id = 4, albumId = 3)
        )

        // When
        val insertTestObserver = albumDao.insertAll(albumItemList).test()
        val getTestObserver = albumDao.getAllItems().test()

        // Then
        insertTestObserver.assertResult(albumItemList.map { it.id.toLong() })
        getTestObserver.assertResult(albumItemList)
    }

    @Test
    fun should_insert_and_count_items() {
        // Given
        val albumItemList: List<AlbumItemEntity> = listOf(
            item.copy(id = 1, albumId = 1),
            item.copy(id = 2, albumId = 1),
            item.copy(id = 3, albumId = 2),
            item.copy(id = 4, albumId = 3)
        )

        // When
        val insertTestObserver = albumDao.insertAll(albumItemList).test()
        val countTestObserver = albumDao.count().test()

        // Then
        insertTestObserver.assertResult(albumItemList.map { it.id.toLong() })
        countTestObserver.assertResult(albumItemList.size)
    }

    @Test
    fun should_insert_and_delete_items() {
        // Given
        val albumItemList: List<AlbumItemEntity> = listOf(
            item.copy(id = 1, albumId = 1),
            item.copy(id = 2, albumId = 1),
            item.copy(id = 3, albumId = 2),
            item.copy(id = 4, albumId = 3)
        )

        // When
        val insertTestObserver = albumDao.insertAll(albumItemList).test()
        val deleteTestObserver = albumDao.deleteAlbumItems().test()
        val countTestObserver = albumDao.count().test()

        // Then
        insertTestObserver.assertResult(albumItemList.map { it.id.toLong() })
        deleteTestObserver.assertResult(albumItemList.size)
        countTestObserver.assertResult(0)
    }

    private val item = AlbumItemEntity(id = 1, albumId = 1, thumbnailUrl = "", url = "", title = "")
}
